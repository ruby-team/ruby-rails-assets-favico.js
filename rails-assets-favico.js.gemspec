# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-favico.js/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-favico.js"
  spec.version       = RailsAssetsFavicoJs::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "Favico.js is a library to manipulate the favicon, adding alert badges, render images or videos."
  spec.summary       = "Favico.js is a library to manipulate the favicon, adding alert badges, render images or videos."
  spec.homepage      = "http://lab.ejci.net/favico.js"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
